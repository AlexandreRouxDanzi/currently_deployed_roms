#!/bin/bash

echo "| DEVICE | VERSION | RELEASE STATION | PREVERSION |
| ------ | ------ | ------ | ------ |"
for device in *; do
 if [ -d $device ]
 then
 cd $device
 declare -A biggest_version
 biggest_version[major]=0
 biggest_version[minor]=0
 biggest_version[patch]=0
 biggest_version[version_total]=0 
 biggest_version[timestamp]=0

 declare -A biggest_preversion
 biggest_preversion[major]=0
 biggest_preversion[minor]=0
 biggest_preversion[patch]=0
 biggest_preversion[prerelease]=""
 biggest_preversion[prerelease_int]=0
 biggest_preversion[prerelease_version]=0
 biggest_preversion[preversion_total]=0
 biggest_preversion[version_total]=0
 biggest_preversion[timestamp]=0

 for update in *; do

 if [[ $update == "e-"*".zip"  ]]
 then
 
 
 #PRODUCT_PRERELEASE=`echo $TAG_NAME |  sed -E 's/v[0-9]*\.[0-9]*(\.[0-9]*)?-(beta|alpha|rc).*/\2/'`
 sedreg='e-([0-9]*)(\.([0-9]*))?(\.([0-9]*))?(-(beta|alpha|rc)(\.([0-9]*))?)?.*-([0-9]*).*'
 major=`echo "$update" | sed -E 's/'$sedreg'/\1/'`
 minor=`echo "$update" | sed -E 's/'$sedreg'/\3/'`
 patch=`echo "$update" | sed -E 's/'$sedreg'/\5/'`
 
 if [ "$patch" = "" ]
 then
  patch=0
  fi
 
  prerelease=`echo "$update" | sed -E 's/'$sedreg'/\7/'`
  if [ "$prerelease" = "alpha" ]
 then
  prerelease_int=0
  elif [ "$prerelease" = "beta" ]
 then
  prerelease_int=1
  elif [ "$prerelease" = "rc" ]
 then
  prerelease_int=2
  else
  prerelease_int=3
  fi
 prerelease_version=`echo "$update" | sed -E 's/'$sedreg'/\9/'`
  
 if [ "$prerelease_version" = "" ]
 then
  prerelease_version=0
  fi
 version_total=$((major*1000000+minor*1000+patch))
 preversion_total=$((prerelease_int*1000+prerelease_version))
 timestamp=`echo "$update" | sed -E 's/e-.*-([0-9]*)-.*/\1/'`

	if [ "$prerelease_int" -ge "3" ]
	then
		if [ "$version_total" -ge "${biggest_version[version_total]}" ]
		then
			deployed=true
			if [ -f "$update.config.json" ]
			then
				percentage=`cat "$update.config.json" | jq -r '.rollout.percentage'`
				if [ "$percentage" -le "0" ]
				then
					deployed=false
				fi
			fi
			if [ $deployed == true ]
			then
				biggest_version[version_total]=$version_total
				biggest_version[major]=$major
				biggest_version[minor]=$minor
				biggest_version[patch]=$patch
				if [ "$timestamp" -ge "${biggest_version[timestamp]}" ]
				then
					biggest_version[timestamp]=$timestamp
					biggest_version[filename]=$update
				fi 
			fi
		fi
	else
			if [ "$version_total" -ge "${biggest_preversion[version_total]}" ]
				then
					if [ "$preversion_total" -ge "${biggest_preversion[preversion_total]}" ]
					then

						biggest_preversion[preversion_total]=$preversion_total
						biggest_preversion[major]=$major
						biggest_preversion[minor]=$minor
						biggest_preversion[patch]=$patch
						biggest_preversion[prerelease]=$prerelease
						biggest_preversion[prerelease_version]=$prerelease_version
						if [ "$timestamp" -ge "${biggest_preversion[timestamp]}" ]
						then
							biggest_preversion[timestamp]=$timestamp
							biggest_preversion[filename]=$update
						fi
					fi
			fi
		fi
	fi



done

releasestation=""
if [ -f e-latest-$device*.prop ]; then
  update=e-`cat e-latest-$device*.prop | grep ro.lineage.version | sed -E 's/ro.lineage.version=(.*)/\1/'`
  major=`echo "$update" | sed -E 's/'$sedreg'/\1/'`
  minor=`echo "$update" | sed -E 's/'$sedreg'/\3/'`
  patch=`echo "$update" | sed -E 's/'$sedreg'/\5/'`
  if [ "$patch" = "" ]
  then
    patch=0
  fi
  releasestation=$major.$minor.$patch
fi
echo "| $device | [${biggest_version[major]}.${biggest_version[minor]}.${biggest_version[patch]}](https://images.ecloud.global/$1/$device/${biggest_version[filename]}) | $releasestation | ${biggest_preversion[major]}.${biggest_preversion[minor]}.${biggest_preversion[patch]}-${biggest_preversion[prerelease]}.${biggest_preversion[prerelease_version]} |"
# echo for $device ${biggest_version[major]}.${biggest_version[minor]}.${biggest_version[patch]} ${biggest_version[filename]}
# echo for $device preversion ${biggest_preversion[major]}.${biggest_preversion[minor]}.${biggest_preversion[patch]}-${biggest_preversion[prerelease]}.${biggest_preversion[prerelease_version]}
cd ..
fi
done
